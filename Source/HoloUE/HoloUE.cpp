// Copyright Epic Games, Inc. All Rights Reserved.

#include "HoloUE.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HoloUE, "HoloUE" );
