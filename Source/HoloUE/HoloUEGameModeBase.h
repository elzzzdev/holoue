// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HoloUEGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOLOUE_API AHoloUEGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
